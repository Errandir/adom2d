using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCharacter : MonoBehaviour,IDamagable
{
    protected static Vector3[] Directions = new Vector3[] {Vector3.left, Vector3.right, Vector3.down, Vector3.up, new Vector3(1f, 1f, 0f), new Vector3(-1f, 1f, 0f),
        new Vector3(-1f, -1f, 0f), new Vector3(1f, -1f, 0f)};
    public NPC_SO NPCScriptableObject;
    protected SpriteRenderer spriteRenderer;
    [SerializeField]
    protected int Hitpoints;
    [SerializeField]
    protected int MaxHitpoints;
    [SerializeField]
    protected int ManaPoints;
    [SerializeField]
    protected int MaxManaPoints;
    [SerializeField]
    protected int Damage;
    [SerializeField]
    protected int Armor;
    [SerializeField]
    protected int Gold;
    [SerializeField]
    protected int Experience;
    public bool IsAggressive;
    public bool IsMoving;
    protected LayerMask obstacles;
    [HideInInspector]
    public Vector3 moveTo;


    protected virtual void Awake()
    {

        Hitpoints = NPCScriptableObject.Hitpoints;
        MaxHitpoints = NPCScriptableObject.MaxHitpoints;
        ManaPoints = NPCScriptableObject.Manapoints;
        MaxManaPoints = NPCScriptableObject.MaxManapoints;
        Damage = NPCScriptableObject.Damage;
        Armor = NPCScriptableObject.Armor;
        Gold = NPCScriptableObject.Gold;
        Experience = NPCScriptableObject.ExpPoints;
        moveTo = gameObject.transform.position;
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = NPCScriptableObject.Sprite;
    }
    protected virtual void Start()
    {

    }
    public virtual void ReceiveDamage(int dmg)
    {
        Debug.Log(NPCScriptableObject.ObjectName + " took " + dmg + " damage.");
        Hitpoints -= dmg;
    }
    protected void Move(Vector3 dir)
    {
        //Adding final vector to list if it is not already occupied
        if (!GameManager.NPCTargetVectors.Contains(moveTo+dir))
        {
            GameManager.NPCTargetVectors.Add(moveTo + dir);
        }
        else
        {
            //Skipping movement if it IS occupied
            return;
        }
        
        if (!Physics2D.OverlapCircle(moveTo + dir, 0.2f, LayerMask.GetMask(GameConst.LayerUnpassable,GameConst.LayerCharacters)))
        {
            moveTo += dir;
        }
    }
    protected void MoveTo(Vector3 start, Vector3 target)
    {
        transform.position = target;
    }
    protected void CheckDirection(Vector3 dir)
    {
        var coll = Physics2D.OverlapCircle(dir, 0.2f);
        if (coll!=null)
        {
            if (coll.TryGetComponent(out NPC npc))
            {
                Debug.Log(npc.NPCScriptableObject.ObjectName + " is in that direction"); 
                //Damaging NPC
                npc.ReceiveDamage(CalculateDamage(GameManager.Instance.player.NPCScriptableObject.Damage,npc.NPCScriptableObject.Armor));
            }
        }
    }

    protected int CalculateDamage(int damage, int armor)
    {
        int damageDealt= damage-armor;
        if (damageDealt < 0)
            return 0;
        return damageDealt;
    }
    public int GetHealth()
    {
        return Hitpoints;
    }
    public int GetMaxHealth()
    {
        return MaxHitpoints;
    }
    public int GetManaPoints()
    {
        return ManaPoints;
    }
    public int GetMaxManaPoints()
    {
        return MaxManaPoints;
    }
    public int GetArmor()
    {
        return Armor;
    }
    public int GetDamage()
    {
        return Damage;
    }
    public int GetGold()
    {
        return Gold;
    }
    public void AddGold(int amount)
    {
        Gold+=amount;
    }

    public int GetExp()
    {
        return Experience;
    }
}
