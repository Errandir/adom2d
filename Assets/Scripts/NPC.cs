using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class NPC : BaseCharacter,ITalker
{
    //Default random movement
    public void MoveNPC()
    {
        Move(SelectRandomMovementDirection());
        MoveTo(transform.position, moveTo);
    }
    //Chasing the player
    public void MoveTowardsPlayer()
    {
        int x=0, y=0;
        float distance = Vector3.Distance(GameManager.Instance.player.transform.position, transform.position);
        Vector3 direction = CalculateVectorToPlayer();
        //Reducing vector values to 1 or -1
        if (direction.x<0)
        {
            x = -1;
        }
        else if (direction.x>0)
        {
            x = 1;
        }
        if (direction.y < 0)
        {
            y = -1;
        }
        else if (direction.y > 0)
        {
            y = 1;
        }
        direction=new Vector3(x,y,0f);
        //If NPC is close to the player he damages him
        if (distance<=1.5f)
        {
            //Attack player
            GameManager.Instance.player.ReceiveDamage(CalculateDamage(NPCScriptableObject.Damage, GameManager.Instance.player.GetArmor()));
        }
        else //Continue moving towards player
        {
            Vector3 prevPosition = moveTo;
            Move(direction);
            //If NPC can't move directly to player he tries stepping to random direction to try to find the path to the player
            if (moveTo==prevPosition)
                Move(SelectRandomMovementDirection());
            MoveTo(transform.position, moveTo);
        }
    }

    private Vector3 CalculateVectorToPlayer()
    {
        Vector3 direction=GameManager.Instance.player.transform.position-transform.position;
        return direction;
    }
    private Vector3 SelectRandomMovementDirection()
    {
        int i = Random.Range(0, Directions.Length);
        return Directions[i];
    }
    public void Talk()
    {
        //Checking if NPC has a dialog option
        if (NPCScriptableObject.Reply != null)
        {
            //Enabling the talking canvas, finding its child objects and setting the sprite and the name of the collocutor
            GameManager.Instance.TalkingCanvas.SetActive(true);
            GameObject talkingPanel = GameManager.Instance.TalkingCanvas.transform.Find("TalkingPanel").gameObject;
            talkingPanel.transform.Find("NPCFace").gameObject.GetComponent<Image>().sprite= spriteRenderer.sprite;
            talkingPanel.transform.Find("NPCName").GetComponent<Text>().text = NPCScriptableObject.ObjectName+", "+ NPCScriptableObject.Role;
            talkingPanel.transform.Find("ReplyText").GetComponent<Text>().text = NPCScriptableObject.Reply;
        }
    }

    public override void ReceiveDamage(int amount)
    {
        base.ReceiveDamage(amount);
        Debug.Log(NPCScriptableObject.ObjectName + " hitpoins = " + Hitpoints);
        GameManager.Instance.ShowText(NPCScriptableObject.ObjectName + " hitpoins = " + Hitpoints, 25, Color.blue, FontStyle.Bold, transform.position, Vector3.up * 25, 1.5f);
        if (Hitpoints<=0)
        {
            //Give exp
            GameManager.Instance.player.AddExp(Experience);
            Observer.OnExpChanged(Experience);
            //Drop gold
            GameManager.Instance.DropLoot(Gold,gameObject.transform.position);
            //Destroy NPC gameobject
            Debug.Log(NPCScriptableObject.ObjectName + " is dead.");
            GameManager.Instance.npcs.Remove(gameObject.GetComponent<NPC>());
            Destroy(gameObject);
        }
    }
}
