using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIControl : MonoBehaviour
{
    private Canvas canvas;
    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartNewGame()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetFloat("X", GameManager.Instance.player.NPCScriptableObject.Position.x);
        PlayerPrefs.SetFloat("Y", GameManager.Instance.player.NPCScriptableObject.Position.y);
        PlayerPrefs.SetFloat("Z", GameManager.Instance.player.NPCScriptableObject.Position.z);
        SceneManager.LoadScene(GameConst.MainSceneName);
        canvas.enabled = false;
    }
}
