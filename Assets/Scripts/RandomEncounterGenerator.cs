using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class RandomEncounterGenerator : MonoBehaviour
{
    public float Rate;
    void Awake()
    {
        Observer.PlayerFinishedMoving += OnPlayerFinishedMoving;
    }

    private void OnPlayerFinishedMoving()
    {
        if (SceneManager.GetActiveScene().name==GameConst.MainSceneName)
            GenerateRandomEncounter();
    }

    private void GenerateRandomEncounter()
    {
        int i = Random.Range(0, 100);
        if (i<=Rate)
        {
            Debug.Log("Random Encounter!");
        }
    }
}
