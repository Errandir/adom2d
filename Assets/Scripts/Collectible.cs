using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : Collidable
{
    public Item CollectibleItem;
    public int Quantity;
    protected override void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = CollectibleItem.Sprite;
    }

    protected virtual void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag==GameConst.PlayerTag)
        {
            OnCollect();
        }

    }
    protected virtual void OnCollect()
    {
        Debug.Log("Collected " + CollectibleItem.ItemTypeObject);
        if (CollectibleItem.ItemTypeObject==Item.ItemType.GOLD)
        {
            GameManager.Instance.player.AddGold(Quantity);
            Observer.OnGoldChanged(Quantity);
            GameManager.Instance.ShowText("Collected" + Quantity.ToString() + " " + CollectibleItem.ItemTypeObject, 25, Color.blue, FontStyle.Bold, transform.position, Vector3.up * 25, 1.5f);
        }

        if (CollectibleItem.ItemTypeObject == Item.ItemType.POTION)
        {
            GameManager.Instance.player.AddHealth(Quantity);
            Observer.OnHPChanged(Quantity);
            GameManager.Instance.ShowText("Collected health "  + " " + CollectibleItem.ItemTypeObject+", gained "+ Quantity.ToString()+" HP", 25, Color.blue, FontStyle.Bold, transform.position, Vector3.up * 25, 1.5f);
        }
        Destroy(gameObject);
    }
}
