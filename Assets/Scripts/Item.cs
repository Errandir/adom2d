using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Item")]
public class Item : ScriptableObject
{
    public string ObjectName;
    public Sprite Sprite;
    public bool Stackable;
    public int Quantity;
    public ItemType ItemTypeObject;
    public enum ItemType
    {
        GOLD, WEAPON, ARMOR, POTION, ITEM
    }
}
