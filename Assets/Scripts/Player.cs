﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class Player : BaseCharacter,IDamagable
{
    public static Player Instance;
    private string dungeonName="";
    public SpriteRenderer actionFrame;
    public Sprite TalkFrameSprite, AttackFrameSprite;
    [SerializeField]
    private bool touchingDungeon = false;
    [SerializeField]
    private bool talkingMode = false;
    [SerializeField]
    private bool attackingMode = false;
    private bool actionCanvasActive = false;
    private Collider2D roofsCollider2D;
    private bool isRoofShown = true;
    
    protected override void Awake()
    {
        base.Awake();
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        
    }
    protected override void Start()
    {
        actionFrame.enabled = talkingMode;
    }

    void Update()
    {
        if (talkingMode)
        {
            actionFrame.enabled = talkingMode;
        }
        else if (attackingMode)
        {
            actionFrame.enabled = attackingMode;
        }
        else
        {
            actionFrame.enabled=false;
        }
        //Forbidding reading keys in talking mode
        if (!talkingMode&&actionCanvasActive) return;
        //Forbidding reading keys when it is not players turn
        if (!GameManager.Instance.playersTurn) return;
        //onPlayerFinishedMoving?.Invoke();
        ReadKeys(talkingMode);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            EnterDungeon();
        }
        //Keys active in dungeons only
        if (SceneManager.GetActiveScene().name != GameConst.MainSceneName &&
            SceneManager.GetActiveScene().name != "MainMenu")
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                SceneManager.LoadScene(GameConst.MainSceneName);
            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                //Start talking to NPC
                actionFrame.GetComponent<SpriteRenderer>().sprite = TalkFrameSprite;
                talkingMode = true;
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                //Attacking NPC
                actionFrame.GetComponent<SpriteRenderer>().sprite = AttackFrameSprite;
                attackingMode = true;
            }
        }
        
    }

    public void EndTalkingMode()
    {
        talkingMode = false;
        actionCanvasActive = false;
    }
    public void StarTalkingMode()
    {
        talkingMode = true;
        actionCanvasActive = true;
    }
    private new void Move(Vector3 dir)
    {
        Collider2D coll = IsNpcInThatDirection(moveTo + dir);
        //Checking if there is a NPC in the direction of moving
        if (coll!=null)
        {
            if (coll.TryGetComponent(out NPC npc))
            {
                Debug.Log(npc.NPCScriptableObject.ObjectName + " is in that direction");
                //If NPC is hostile
                if (npc.IsAggressive)
                {
                    //Damaging NPC
                    npc.ReceiveDamage(CalculateDamage(GameManager.Instance.player.NPCScriptableObject.Damage, npc.NPCScriptableObject.Armor));
                }
            }
        }
        else if (!Physics2D.OverlapCircle(moveTo + dir, 0.2f, LayerMask.GetMask(GameConst.LayerUnpassable, GameConst.LayerCharacters)))
        {
            moveTo += dir;
        }
        MoveTo(transform.position, moveTo);
        ShowRoofs();
        OnPlayerFinishedMoving();
    }
    private Collider2D IsNpcInThatDirection(Vector3 dir)
    {
        var coll = Physics2D.OverlapCircle(dir, 0.2f, LayerMask.GetMask(GameConst.LayerCharacters));
        if (coll != null)
            return coll;
        return null;
    }

    private void OnPlayerFinishedMoving()
    {
        GameManager.Instance.playersTurn = false;
        Observer.OnPlayerFinishedMoving();
    }
    private void ShowRoofs()
    {
        var coll = Physics2D.OverlapCircle(gameObject.transform.position, 0.2f,
            LayerMask.GetMask(GameConst.LayerRoofs));
        if (coll!=null)
        {
            roofsCollider2D = coll;
            isRoofShown = false;
            coll.gameObject.GetComponent<TilemapRenderer>().enabled=false;
        }
        else
        {
            if (roofsCollider2D != null&&!isRoofShown)
            {
                isRoofShown = true;
                roofsCollider2D.gameObject.GetComponent<TilemapRenderer>().enabled = true;
                roofsCollider2D = null;
            }
        }
    }
    private void MakeNpcAgressive(Vector3 dir)
    {
        Collider2D coll = IsNpcInThatDirection(moveTo + dir);
        //Checking if there is a NPC in the direction of moving
        if (coll != null)
        {
            if (coll.TryGetComponent(out NPC npc))
            {
                Debug.Log(npc.NPCScriptableObject.ObjectName + " is in that direction");
                //If NPC is hostile
                npc.IsAggressive = true;
            }
        }
        /*else
        {
            Debug.Log("There is nobody to attack there! ");
        }*/
        attackingMode = false;
    }
    private void ReadKeys(bool isTalking)
    {
        if (!isTalking)
        {
            GameManager.NPCTargetVectors.Clear();

            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                if (attackingMode)
                {
                    MakeNpcAgressive(Vector3.left);
                }
                Move(Vector3.left);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad6))
            {
                if (attackingMode)
                {
                    MakeNpcAgressive(Vector3.right);
                }
                Move(Vector3.right);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                if (attackingMode)
                {
                    MakeNpcAgressive(Vector3.down);
                }
                Move(Vector3.down);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad8))
            {
                if (attackingMode)
                {
                    MakeNpcAgressive(Vector3.up);
                }
                Move(Vector3.up);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad9))
            {
                if (attackingMode)
                {
                    MakeNpcAgressive(new Vector3(1f, 1f, 0f));
                }
                Move(new Vector3(1f, 1f, 0f));
            }
            else if (Input.GetKeyDown(KeyCode.Keypad7))
            {
                if (attackingMode)
                {
                    MakeNpcAgressive(new Vector3(-1f, 1f, 0f));
                }
                Move(new Vector3(-1f, 1f, 0f));
            }
            else if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                if (attackingMode)
                {
                    MakeNpcAgressive(new Vector3(-1f, -1f, 0f));
                }
                Move(new Vector3(-1f, -1f, 0f));
            }
            else if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                if (attackingMode)
                {
                    MakeNpcAgressive(new Vector3(1f, -1f, 0f));
                }
                Move(new Vector3(1f, -1f, 0f));
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                TalkTo(Vector3.left);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad6))
            {
                TalkTo(Vector3.right);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                TalkTo(Vector3.down);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad8))
            {
                TalkTo(Vector3.up);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad9))
            {
                TalkTo(new Vector3(1f, 1f, 0f));
            }
            else if (Input.GetKeyDown(KeyCode.Keypad7))
            {
                TalkTo(new Vector3(-1f, 1f, 0f));
            }
            else if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                TalkTo(new Vector3(-1f, -1f, 0f));
            }
            else if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                TalkTo(new Vector3(1f, -1f, 0f));
            }
        }
    }

    private void TalkTo(Vector3 direction)
    {
        Vector3 talkerPosition = transform.position + direction;
        var coll = Physics2D.OverlapCircle(talkerPosition, 0.2f, LayerMask.GetMask(GameConst.LayerCharacters));
        if (coll!=null)
        {
            ITalker iTalker=coll.GetComponent<ITalker>();
            if (iTalker!=null)
            {
                Debug.Log("Talking to NPC");
                iTalker.Talk();
            }
        }
        else
        {
            Debug.Log("There is nobody to talk to there! ");
        }
        talkingMode = false;
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == GameConst.Dungeon)
        {
            touchingDungeon = true;
            Portal portal = other.gameObject.GetComponent<Portal>();
            dungeonName = portal.SceneName;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == GameConst.Dungeon)
        {
            touchingDungeon = false;
            dungeonName = "";
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<IDamagable>()!=null)
        {
            Debug.Log("Player damaged by " + other.gameObject.name);
        }
    }

    private void EnterDungeon()
    {
        if (touchingDungeon)
        {
            touchingDungeon = false;
            SceneManager.LoadScene(dungeonName);
        }
    }

    public override void ReceiveDamage(int amount)
    {
        Debug.Log(NPCScriptableObject.ObjectName + " took " + amount + " damage.");
        Hitpoints -= amount;
        Observer.OnPlayerDamaged(Hitpoints);
        if (Hitpoints <= 0)
        {
            //End of game
            Debug.Log("Player is dead");
        }
    }

    public void AddHealth(int amount)
    {
        Hitpoints += amount;
        if (Hitpoints > MaxHitpoints)
        {
            Hitpoints = MaxHitpoints;
        }
    }

    public void AddExp(int amount)
    {
        Experience += amount;
    }
}
