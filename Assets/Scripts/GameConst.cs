using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConst
{
    public const string FloatingTextManager = "FloatingTextManager";
    public const string Dungeon = "Dungeon";
    public const string NPC = "NPC";
    public const string LayerUnpassable = "Unpassable";
    public const string LayerCharacters = "Characters";
    public const string LayerRoofs = "Roofs";
    public const string MainSceneName = "Main";
    /*public const string LayerPlayer = "Player";
    public const string HealthTag = "Health";
    public const string DoubleJumpTag = "DoubleJump";
    public const string GunTag = "Gun";*/
    public const string PlayerTag = "Player";
    /*public const string TurnOffTag = "TurnOff";
    public const string DestructibleTag = "Destructible";
    public const string GameManagerTag = "GameManager";

    public const string ScoreFilePath = "Assets/score.dat";*/
}
