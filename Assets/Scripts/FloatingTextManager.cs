using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingTextManager : MonoBehaviour
{
    public static FloatingTextManager Instance;
    public GameObject TextContainer;
    public GameObject TextPrefab;
    private List<FloatingText> floatingTexts=new List<FloatingText>();

    void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        foreach (FloatingText txt in floatingTexts)
        {
            txt.UpdateFloatingText();
        }
    }
    public void Show(string message,int fontSize,Color color,FontStyle fontStyle, Vector3 position, Vector3 motion, float duration)
    {
        FloatingText floatingText = GetFloatingText();
        floatingText.Text.text = message;
        floatingText.Text.fontSize = fontSize;
        floatingText.Text.color = color;
        floatingText.Text.fontStyle = fontStyle;
        floatingText.GO.transform.position = Camera.main.WorldToScreenPoint(position);
        floatingText.Motion = motion;
        floatingText.Duration = duration;
        floatingText.Show();
    }
    private FloatingText GetFloatingText()
    {
        FloatingText txt = floatingTexts.Find(t => !t.Active);
        if (txt == null)
        {
            txt=new FloatingText();
            txt.GO = Instantiate(TextPrefab);
            txt.GO.transform.SetParent(TextContainer.transform);
            txt.Text = txt.GO.GetComponent<Text>();
            floatingTexts.Add(txt);
        }
        return txt;
    }
}
