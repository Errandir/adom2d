using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectsLoader : MonoBehaviour
{
    [System.Serializable]
    public class DungeonToInstantiate
    {
        public Vector3 Position;
        public GameObject Object;
        public string SceneName;
        public int VisionRadius;
    }
    [System.Serializable]
    public class DungeonList
    {
        public DungeonToInstantiate[] Dungeons;
    }

    [System.Serializable]
    public class ItemToInstantiate
    {
        public Vector3 Position;
        public GameObject Object;
        public string ScriptableObject;
        public int Quantity;
    }
    [System.Serializable]
    public class ItemList
    {
        public ItemToInstantiate[] Items;
    }

    [System.Serializable]
    public class NPCToInstantiate
    {
        public Vector3 Position;
        public GameObject Object;
        public string ScriptableObject;
        public bool IsAgressive;
        public bool IsMoving;
    }
    [System.Serializable]
    public class NPCList
    {
        public NPCToInstantiate[] NPCs;
    }
    public GameObject DungeonGameObject, NpcGameObject,ItemGameObject;
    public DungeonList Dungeon = new DungeonList();
    public ItemList Item= new ItemList();
    public NPCList NPC = new NPCList();
    private string sceneName;
    void Awake()
    {
        sceneName = SceneManager.GetActiveScene().name;
        InstantiateDungeons();
        InstantiateItems();
        InstantiateNPCs();
    }
    private void InstantiateDungeons()
    {
        string dungeonFilePath = Application.dataPath + "/JSONs/" + sceneName + "/DungeonsToLoad.json";
        if (!File.Exists(dungeonFilePath)) return;
        string dungeonJson = File.ReadAllText(dungeonFilePath);
        Dungeon = JsonUtility.FromJson<DungeonList>(dungeonJson);
        //Instantiating dungeons
        for (int i = 0; i < Dungeon.Dungeons.Length; i++)
        {
            DungeonToInstantiate obj = Dungeon.Dungeons[i];
            obj.Object = DungeonGameObject;
            obj.Object.GetComponent<Portal>().SceneName = obj.SceneName;
            obj.Object.GetComponent<Portal>().VisionRadius = obj.VisionRadius;
            Instantiate(obj.Object, obj.Position, Quaternion.identity);
        }
    }

    private void InstantiateItems()
    {
        string itemFilePath = Application.dataPath + "/JSONs/" + sceneName + "/ItemsToLoad.json";
        if (!File.Exists(itemFilePath)) return;
        string itemJson = File.ReadAllText(itemFilePath);
        Item = JsonUtility.FromJson<ItemList>(itemJson);
        //Instantiating collectible items
        for (int i = 0; i < Item.Items.Length; i++)
        {
            ItemToInstantiate item = Item.Items[i];
            item.Object = ItemGameObject;
            GameObject g=Instantiate(item.Object, item.Position, Quaternion.identity);
            foreach (var scriptableObject in GameManager.Instance.ItemSOList)
            {
                if (scriptableObject.name == item.ScriptableObject)
                {
                    g.GetComponent<Collectible>().CollectibleItem = scriptableObject;
                    g.GetComponent<Collectible>().Quantity = item.Quantity;
                }
            }
        }
    }

    private void InstantiateNPCs()
    {
        string npcFilePath = Application.dataPath + "/JSONs/" + sceneName + "/NPCsToLoad.json";
        if (!File.Exists(npcFilePath)) return;

        string npcJson = File.ReadAllText(npcFilePath);
        NPC = JsonUtility.FromJson<NPCList>(npcJson);
        //Instantiating NPCs
        for (int i = 0; i < NPC.NPCs.Length; i++)
        {
            NPCToInstantiate npc = NPC.NPCs[i];
            npc.Object = NpcGameObject;
            foreach (NPC_SO scriptableObject in GameManager.Instance.NpcSOList)
            {
                if (scriptableObject.name == npc.ScriptableObject)
                {
                    npc.Object.GetComponent<NPC>().NPCScriptableObject = scriptableObject;
                    npc.Object.GetComponent<NPC>().IsAggressive = npc.IsAgressive;
                    npc.Object.GetComponent<NPC>().IsMoving = npc.IsMoving;
                }
            }
            Instantiate(npc.Object, npc.Position, Quaternion.identity);
        }
    }
}
