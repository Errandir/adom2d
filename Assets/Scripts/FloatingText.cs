using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText
{
    public bool Active;
    public GameObject GO;
    public Text Text;
    public Vector3 Motion;
    public float Duration, LastShown;

    public void Show()
    {
        Active = true;
        LastShown = Time.time;
        GO.SetActive(Active);
    }

    public void Hide()
    {
        Active = false;
        GO.SetActive(Active);
    }

    public void UpdateFloatingText()
    {
        if (!Active)
        {
            return;
        }

        if (Time.time-LastShown>Duration)
        {
            Hide();
        }

        GO.transform.position += Motion * Time.deltaTime;
    }
}
