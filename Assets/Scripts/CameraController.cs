using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance;
    public Transform lookAt;
    private Player player;
    void Awake()
    {

        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }
    void Start()
    {
        player = GameObject.FindGameObjectWithTag(GameConst.PlayerTag).GetComponent<Player>();
        //player = GameManager.Instance.player;
        //lookAt = player.transform.position;
        //lookAt = GameObject.FindObjectOfType<Player>().transform;
    }
    void LateUpdate()
    {
        if (lookAt == null)
        {
            lookAt = GameObject.FindObjectOfType<Player>().transform;
        }
        transform.position = new Vector3(lookAt.transform.position.x, lookAt.transform.position.y, -10);
    }

    public void OnSceneLoaded(Scene sc, LoadSceneMode mode)
    {
        player = GameObject.FindGameObjectWithTag(GameConst.PlayerTag).GetComponent<Player>();
        lookAt = GameObject.FindObjectOfType<Player>().transform;
        transform.position = lookAt.transform.position;
        //player= GameManager.Instance.player;
        //lookAt = player.transform.position;
    }
    private void OnSceneUnloaded(Scene current)
    {
        //SceneManager.sceneLoaded -= OnSceneLoaded;
        //SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }
}
