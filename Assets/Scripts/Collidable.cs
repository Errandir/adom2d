using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(BoxCollider2D))]
public class Collidable : MonoBehaviour
{
    private BoxCollider2D boxCollider2D;

    void Awake()
    {
        boxCollider2D = gameObject.AddComponent<BoxCollider2D>() as BoxCollider2D;
        boxCollider2D.size=new Vector2(0.15f,0.15f);
    }
    protected virtual void Start()
    {

    }

    protected virtual void OnCollide(Collider2D collider)
    {

    }
}
