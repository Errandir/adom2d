using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal:MonoBehaviour
{
    public string SceneName;
    public float VisionRadius = 3f;
    [SerializeField] private bool isFoundByPlayer = false;
    private SpriteRenderer spriteRenderer;
    void Awake()
    {
        Observer.PlayerFinishedMoving += OnPlayerFinishedMoving;
        spriteRenderer=GetComponent<SpriteRenderer>();
    }

    void OnDisable()
    {
        Observer.PlayerFinishedMoving -= OnPlayerFinishedMoving;
    }
    void Start()
    {
        Vector3 distance = CalculateDistance();
        if (distance.magnitude <= VisionRadius)
        {
            spriteRenderer.enabled = true;
            //And marking it as 'found'
            isFoundByPlayer = true;
        }
    }

    private void OnPlayerFinishedMoving()
    {
        //If we are not on global map then skip it
        if (SceneManager.GetActiveScene().name != GameConst.MainSceneName) return;
        //If the dungeon is already found by player no more distance checking
        if(isFoundByPlayer) return;
        Vector3 distance = CalculateDistance();
        //If player is closer to dungeon than vision distance - show the dungeon on global map
        //spriteRenderer.enabled = distance.magnitude <= VisionRadius;
        if (distance.magnitude <= VisionRadius)
        {
            spriteRenderer.enabled = true;
            //And marking it as 'found'
            isFoundByPlayer = true;
        }


    }

    private Vector3 CalculateDistance()
    {
        return GameManager.Instance.player.transform.position - transform.position;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, VisionRadius);
        //Gizmos.DrawLine(wallCheckPoint.position, new Vector3(wallCheckPoint.position.x + wallCheckDistance, wallCheckPoint.position.y, wallCheckPoint.position.z));
    }
}
