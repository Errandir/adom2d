using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public static event Action<int> PlayerDamaged;
    public static event Action<int> GoldChanged;
    public static event Action<int> HPChanged;
    public static event Action<int> ExpChanged;
    public static event Action GameOver;
    public static event Action PlayerFinishedMoving;

    public static void OnPlayerDamaged(int damage)
    {
        PlayerDamaged?.Invoke(damage);
    }
    public static void OnGoldChanged(int quantity)
    {
        GoldChanged?.Invoke(quantity);
    }
    public static void OnHPChanged(int quantity)
    {
        HPChanged?.Invoke(quantity);
    }
    public static void OnExpChanged(int quantity)
    {
        ExpChanged?.Invoke(quantity);
    }
    public static void OnGameOver()
    {
        GameOver?.Invoke();
    }
    public static void OnPlayerFinishedMoving()
    {
        PlayerFinishedMoving?.Invoke();
    }
}
