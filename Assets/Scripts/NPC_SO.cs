using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "NPC_SO")]
public class NPC_SO : ScriptableObject
{
    public string ObjectName;
    public string Role;
    public Sprite Sprite;
    public int Hitpoints;
    public int MaxHitpoints;
    public int Manapoints;
    public int MaxManapoints;
    public bool IsAggressive;
    public int Damage;
    public int Armor;
    public int Gold;
    public int ExpPoints;
    public string Reply;
    public Vector3 Position;
}
