using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkingCanvasControl : MonoBehaviour
{
    public static TalkingCanvasControl Instance;
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    void OnDisable()
    {
        Debug.Log("Talking panel was disabled");
        GameManager.Instance.player.EndTalkingMode();
    }

    void OnEnable()
    {
        Debug.Log("Talking panel was enabled");
        GameManager.Instance.player.StarTalkingMode();

    }
    public void ExitTalkingMode()
    {
        gameObject.SetActive(false);
        //GameManager.Instance.player.EndTalkingMode();
    }
}
