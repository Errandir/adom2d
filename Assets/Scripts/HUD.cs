using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    //private Player player;
    public static HUD Instance;
    public Image playerImage;
    public Text nameText, armorText, healthText, damageText,goldText,manaText,expText;
    void Awake()
    {

        if (HUD.Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        Observer.PlayerDamaged += OnPlayerDamaged;
        Observer.GoldChanged += OnGoldChanged;
        Observer.HPChanged += OnHPChanged;
        Observer.ExpChanged += OnExpChanged;
        SceneManager.sceneLoaded += OnSceneLoaded;
        //player = GameObject.FindGameObjectWithTag(GameConst.PlayerTag).GetComponent<Player>();
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        InitialiseHUD();
    }
    private void OnExpChanged(int amount)
    {
        expText.text =GameManager.Instance.player.GetExp().ToString();
    }
    private void OnHPChanged(int amount)
    {
        healthText.text = GameManager.Instance.player.GetHealth().ToString()+"/"+ GameManager.Instance.player.GetMaxHealth();
    }
    private void OnGoldChanged(int quantity)
    {
        goldText.text=GameManager.Instance.player.GetGold().ToString();
    }

    private void OnPlayerDamaged(int damage)
    {
        healthText.text= damage + "/" + GameManager.Instance.player.GetMaxHealth();
    }

    void Start()
    {
        //player = GameObject.FindGameObjectWithTag(GameConst.PlayerTag).GetComponent<Player>();
        /*playerImage = hudPanel.transform.Find("PlayerImage").gameObject.GetComponent<Image>();
        nameText = hudPanel.transform.Find("NameText").GetComponent<Text>();
        armorText = hudPanel.transform.Find("ArmorText").GetComponent<Text>();
        healthText = hudPanel.transform.Find("HPText").GetComponent<Text>();
        damageText = hudPanel.transform.Find("DamageText").GetComponent<Text>();
        goldText = hudPanel.transform.Find("GoldText").GetComponent<Text>();
        manaText = hudPanel.transform.Find("MPText").GetComponent<Text>();*/
        //InitialiseHUD();
    }

    private void InitialiseHUD()
    {
        playerImage.sprite = GameManager.Instance.player.NPCScriptableObject.Sprite;
        nameText.text = GameManager.Instance.player.NPCScriptableObject.ObjectName;
        armorText.text = GameManager.Instance.player.GetArmor().ToString();
        healthText.text = GameManager.Instance.player.GetHealth().ToString() + "/" + GameManager.Instance.player.GetMaxHealth().ToString();
        damageText.text = GameManager.Instance.player.GetDamage().ToString();
        manaText.text = GameManager.Instance.player.GetManaPoints().ToString() + "/" + GameManager.Instance.player.GetMaxManaPoints().ToString();
        goldText.text = GameManager.Instance.player.GetGold().ToString();
        expText.text = GameManager.Instance.player.GetExp().ToString();
    }
}
