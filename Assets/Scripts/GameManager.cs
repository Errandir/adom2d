using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Unity.VisualStudio.Editor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    //public GameObject SpawnPoint;
    public GameObject LootPrefab;
    public static List<Vector3> NPCTargetVectors;
    public GameObject PlayerGO;
    [HideInInspector]
    public Player player;
    //public Camera Camera;
    private FloatingTextManager floatingTextManager;
    //public int Money;
    public int Experience;
    public GameObject[] NpcsGOs;
    public List<NPC> npcs;
    public bool playersTurn = true;
    //[HideInInspector]
    public GameObject TalkingCanvas;
    public Canvas HUD;
    public List<Item> ItemSOList;
    public List<NPC_SO> NpcSOList;
    private void Awake()
    {
        player = PlayerGO.GetComponent<Player>();
        floatingTextManager = GameObject.FindGameObjectWithTag(GameConst.FloatingTextManager).GetComponent<FloatingTextManager>();
        if (GameManager.Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(TalkingCanvas);
        HUD.enabled = false;
        ItemSOList = Resources.LoadAll<Item>("Items").ToList();
        NpcSOList = Resources.LoadAll<NPC_SO>("NPC").ToList();
    }

    private void Start()
    {
        NPCTargetVectors=new List<Vector3>();
    }

    private void Update()
    {
        if (playersTurn)
            return;
        MoveNPCs();
    }

    private void MoveNPCs()
    {
        for (int i = 0; i < npcs.Count; i++)
        {
            if (!npcs[i].IsAggressive)
            {
                if (npcs[i].IsMoving)
                {
                    npcs[i].MoveNPC();
                }
            }
            else
            {
                npcs[i].MoveTowardsPlayer();
            }
        }
        playersTurn = true;
    }
    private void OnSceneUnloaded(Scene current)
    {
        //TD: Switch to PlayerPrefs to keep the initial SO intact

        PlayerPrefs.SetInt("Hitpoints", player.GetHealth());
        PlayerPrefs.SetInt("MaxHitpoints", player.GetMaxHealth());
        PlayerPrefs.SetInt("Armor", player.GetArmor());
        PlayerPrefs.SetInt("Damage", player.GetDamage());
        PlayerPrefs.SetInt("Manapoints", player.GetManaPoints());
        PlayerPrefs.SetInt("MaxManapoints", player.GetMaxManaPoints());
        PlayerPrefs.SetInt("Gold", player.GetGold());
        PlayerPrefs.SetInt("Exp", player.GetExp());
        //Saving data to PlayerSO
        /*player.NPCScriptableObject.Hitpoints = player.GetHealth();
        player.NPCScriptableObject.MaxHitpoints = player.GetMaxHealth();
        player.NPCScriptableObject.Armor = player.GetArmor();
        player.NPCScriptableObject.Damage = player.GetDamage();
        player.NPCScriptableObject.Manapoints = player.GetManaPoints();
        player.NPCScriptableObject.MaxManapoints = player.GetMaxManaPoints();
        player.NPCScriptableObject.Gold = player.GetGold();*/
        if (current.name == GameConst.MainSceneName)
        {
            PlayerPrefs.SetFloat("X",player.transform.position.x);
            PlayerPrefs.SetFloat("Y", player.transform.position.y);
            PlayerPrefs.SetFloat("Z", player.transform.position.z);
        }
            //player.NPCScriptableObject.Position = player.transform.position;
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }
    public void OnSceneLoaded(Scene sc, LoadSceneMode mode)
    {
        //TD: Get rid of finding NPCs - you store them in a scene-specific JSON now. InstantiateNPCs in ObjectsLoader script.
        NpcsGOs = GameObject.FindGameObjectsWithTag(GameConst.NPC);
        npcs=new List<NPC>();
        for (int i = 0; i < NpcsGOs.Length; i++)
        {
            npcs.Add(NpcsGOs[i].GetComponent<NPC>());
        }
        //Hiding player in main menu scene
        GameManager.Instance.player.gameObject.GetComponent<SpriteRenderer>().enabled=sc.name!="MainMenu";
        //Depending on the scene name
        if (sc.name != GameConst.MainSceneName&&sc.name!="MainMenu")
        {
            //In the dungeon player should be instantiated in pre-defined point saved in scene-specific JSONs
            string npcFilePath = Application.dataPath + "/JSONs/" + SceneManager.GetActiveScene().name + "/DungeonParameters.json";
            //if (!File.Exists(npcFilePath)) return;
            string posJson = File.ReadAllText(npcFilePath);
            DungeonParameters dungeonParameters = JsonUtility.FromJson<DungeonParameters>(posJson);
            player.gameObject.transform.position = dungeonParameters.Position;
        }
        else
        {
            //On the main map player is instantiated in the position saved in PlayerPrefs
            if (PlayerPrefs.HasKey("X") && PlayerPrefs.HasKey("Y") && PlayerPrefs.HasKey("Z"))
            {
                float x = PlayerPrefs.GetFloat("X");
                float y = PlayerPrefs.GetFloat("Y");
                float z = PlayerPrefs.GetFloat("Z");
                player.gameObject.transform.position = new Vector3(x, y, z);
            }
            //Showing HUD everywhere except main menu
            HUD.enabled = sc.name!="MainMenu";
        }
        player.moveTo = player.gameObject.transform.position;
    }

    public void ShowText(string msg, int font, Color color, FontStyle fontSt,Vector3 pos, Vector3 motion, float duration)
    {
        floatingTextManager.Show(msg, font, color,fontSt, pos, motion, duration);
    }

    public void DropLoot(int goldAmount, Vector3 position)
    {
        //TD: Loot variations
        if(goldAmount<=0)
            return;
        LootPrefab.GetComponent<Collectible>().Quantity = goldAmount;
        Instantiate(LootPrefab, position, Quaternion.identity);
    }
}
